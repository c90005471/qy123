<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/17
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>成为商标注册管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/static/layui/layui.js" charset="utf-8"></script>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo"> 成为商标注册管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->

        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="${user.get("avatar")}"
                         class="layui-nav-img">
                    ${user.get("user_name")}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料(头像修改)</a></dd>
                    <dd><a href="">安全设置（密码重置）</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/userLogout">注销</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <c:forEach items="${firstMenus}" var="first">

                    <li class="layui-nav-item layui-nav-itemed" >
                        <a class="" href="javascript:;"> <i class="layui-icon ${first.get("icon")}" style="margin-right:8px "></i>${first.get("menu_name")}</a>
                        <c:forEach items="${secondMenus}" var="second">
                            <c:if test="${second.parent_id eq first.menu_id}">
                                <dl class="layui-nav-child">
                                    <dd><a href="${second.url}">${second.menu_name}</a></dd>
                                </dl>
                            </c:if>
                        </c:forEach>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <iframe id="iframMain" src="" style="width: 100%;height: 100%"></iframe>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>

<script>
    //JavaScript代码区域
    layui.use(['element', 'jquery'], function () {
        var element = layui.element;
        var $ = layui.jquery;
        $(function () {
            //页面加载之后执行的代码
            //点击a标签的时候，修改iframe的src属性
            $("dd>a").click(function (e) {
                //阻断超链接的跳转事件
                e.preventDefault();
                //取得超链接对象的href属性值赋给iframe的src属性
                $("#iframMain").attr("src", $(this).attr("href"));
            })
        })
    });
</script>

</body>
</html>
