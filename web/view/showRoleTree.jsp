<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/17
  Time: 15:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>演示layui</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
</head>
<body>
<%--layui的代码块，默认不显示，只有在js中调用的时候显示--%>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
        <button class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del">删除</button>
    </div>
</script>
<%--行内工具栏--%>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<form class="layui-form" action="" id="saveUserForm" style="display: none;padding: 20px">
    <div class="layui-form-item">
        <label class="layui-form-label">用户名</label>
        <div class="layui-input-block">
            <input type="text" name="username" required lay-verify="required" placeholder="请输入用户名" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-block">
            <input type="text" name="password" required lay-verify="required|pass" placeholder="请输入密码" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机号</label>
        <div class="layui-input-block">
            <input type="text" name="phone" required lay-verify="required|phone" placeholder="请输入手机号" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">部门</label>
        <div class="layui-input-block">
            <select id="deptidSelect" name="deptid" lay-verify="required">

            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formSaveUser">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<form class="layui-form" action="" id="updateUserForm" lay-filter="updateUserForm" style="display: none;padding: 20px">
        <input type="hidden" name="id">
    <div class="layui-form-item">
        <label class="layui-form-label">用户名</label>
        <div class="layui-input-block">
            <input type="text" name="username" required lay-verify="required" placeholder="请输入用户名" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-block">
            <input type="text" name="password" required lay-verify="required|pass" placeholder="请输入密码" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机号</label>
        <div class="layui-input-block">
            <input type="text" name="phone" required lay-verify="required" placeholder="请输入手机号" autocomplete="off"
                   class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">部门</label>
        <div class="layui-input-block">
            <select id="deptidSelectUpdate" name="deptid" lay-verify="required">

            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formUpdateUser">修改</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<form class="layui-form" action="" style="padding: 10px">
    <div class="layui-inline">
        用户名
        <div class="layui-input-inline">
            <input type="text" name="username" placeholder="请输入用户名" autocomplete="off"
                   class="layui-input">
        </div>
        手机号
        <div class="layui-input-inline">
            <input type="text" name="phone" placeholder="请输入手机号" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline">
            <button class="layui-btn" lay-submit lay-filter="formDemo">查询</button>
            <button type="reset" onclick="resetSearch()" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<%--filter是选择器--%>
<table id="demo" lay-filter="test"></table>
</body>
<script>
    var table;
    layui.use(['table', 'form', 'layer', 'jquery'], function () {
        table = layui.table;
        var form = layui.form;
        var layer = layui.layer;
        var $ = layui.jquery;

        //第一个实例，渲染
        table.render({
            elem: '#demo'
            , height: 450
            , url: '${pageContext.request.contextPath}/userinfo?method=showUser' //数据接口
            , page: true //开启分页
            , limit: 5//默认一页显示多少行
            , limits: [5, 10, 15, 20, 25, 30]
            , autoSort: false//开启的话是前端自动排序，不跟服务器交互
            , toolbar: '#toolbarDemo' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
            , cols: [[ //表头
                {type: 'checkbox', fixed: 'left'}
                , {field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left'}
                , {field: 'username', title: '用户名', width: 80}
                , {field: 'password', title: '密码', width: 120, sort: true}
                , {field: 'createtime', title: '创建时间', width: 180}
                , {field: 'phone', title: '手机号', width: 177}
                , {field: 'dname', title: '部门名称', width: 177}
                , {fixed: 'right', width: 165, align: 'center', toolbar: '#barDemo'}
            ]]
        });
        //监听排序事件
        table.on('sort(test)', function (obj) { //注：sort 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            console.log(obj.field); //当前排序的字段名
            console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
            console.log(this); //当前排序的 th 对象
            //尽管我们的 table 自带排序功能，但并没有请求服务端。
            //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
            table.reload('demo', {
                initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。
                , where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
                    field: obj.field //排序字段
                    , order: obj.type //排序方式
                }
            });

            //layer.msg('服务端排序。order by '+ obj.field + ' ' + obj.type);
        });

        form.on('submit(formDemo)', function (data) {
            console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            table.reload('demo', {
                where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
                    search_username: data.field.username //查询字段
                    , search_phone: data.field.phone //排序方式

                },
                page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。

        });
        form.on('submit(formSaveUser)', function (data) {
            console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            //提交表单数据
            $.ajax({
                url: "${pageContext.request.contextPath}/userinfo?method=saveUser",
                type: "POST",
                dataType: "JSON",
                data: data.field,
                success: function (data) {
                    if (data.code == 0) {
                        //关闭对话框
                        layer.closeAll(); //疯狂模式，关闭所有层
                        //刷新表格数据
                        table.reload('demo');

                    }
                    layer.msg(data.msg);
                }

            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
        form.on('submit(formUpdateUser)', function (data) {
            console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            //提交表单数据
            $.ajax({
                url: "${pageContext.request.contextPath}/userinfo?method=updateUser",
                type: "POST",
                dataType: "JSON",
                data: data.field,
                success: function (data) {
                    if (data.code == 0) {
                        //关闭对话框
                        layer.closeAll(); //疯狂模式，关闭所有层
                        //刷新表格数据
                        table.reload('demo');

                    }
                    layer.msg(data.msg);
                }

            });

            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。

        });

        //监听头工具栏事件
        table.on('toolbar(test)', function (obj) {
            var checkStatus = table.checkStatus(obj.config.id)
                , data = checkStatus.data; //获取选中的数据
            console.log(data)
            switch (obj.event) {
                case 'add':
                    layer.open({
                        title: "用户添加",
                        type: 1,
                        content: $("#saveUserForm"),
                        area: ['400px', '340px']
                    });
                    //动态加载所有的部门信息，填充
                    //填充部门下拉框deptidSelect
                    $.ajax({
                        url:"${pageContext.request.contextPath}/dept?method=findAllDept",
                        type:"POST",
                        dataType:"JSON",
                        success:function (ret) {
                            if(ret.code==0){
                                //<option value="0">写作</option>
                                var htmlStr="";
                                for (let i = 0; i < ret.data.length; i++) {
                                    htmlStr+="<option value="+ret.data[i].deptid+">"+ret.data[i].dname+"</option>"
                                }
                                console.log(htmlStr)
                                $("#deptidSelect").html(htmlStr);
                                //重新渲染表单
                                form.render('select');
                            }
                        }
                    });
                    break;
                case 'update':
                    if (data.length === 0) {
                        layer.msg('请选择一行');
                    } else if (data.length > 1) {
                        layer.msg('只能同时编辑一个');
                    } else {
                        layer.alert('编辑 [id]：' + checkStatus.data[0].id);
                    }
                    break;
                case 'del':
                    if (data.length === 0) {
                        layer.msg('请选择一行');
                    } else {
                        //定义一个数组，封装所有的id
                        var ids = [];
                        //循环所有选中的行
                        for (let i = 0; i < data.length; i++) {
                            ids[i] = data[i].id;
                        }
                        layer.confirm('真的删除行么', function (index) {
                            //去后台批量删除数据
                            $.ajax({
                                url: "${pageContext.request.contextPath}/userinfo?method=delUserById",
                                type: "POST",
                                dataType: "JSON",
                                data: {id: ids.join(",")},
                                success: function (data) {
                                    if (data.code == 0) {
                                        //关闭对话框
                                        layer.closeAll(); //疯狂模式，关闭所有层
                                        //刷新表格数据
                                        table.reload('demo');

                                    }
                                    layer.msg(data.msg);
                                }

                            });
                        });
                    }
                    break;
            }
            ;
        });
        //监听行工具事件
        table.on('tool(test)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
            console.log(data)
                , layEvent = obj.event; //获得 lay-event 对应的值
            if (layEvent === 'detail') {
                layer.msg('查看操作');
            } else if (layEvent === 'del') {
                layer.confirm('真的删除行么', function (index) {
                    //删除数据库中
                    $.ajax({
                        url: "${pageContext.request.contextPath}/userinfo?method=delUserById",
                        type: "POST",
                        dataType: "JSON",
                        data: {id: data.id},
                        success: function (data) {
                            if (data.code == 0) {
                                //关闭对话框
                                layer.closeAll(); //疯狂模式，关闭所有层
                                //刷新表格数据
                                table.reload('demo');

                            }
                            layer.msg(data.msg);
                        }

                    });
                });
            } else if (layEvent === 'edit') {
                //给表单赋值
                form.val('updateUserForm', {
                    "username": data.username // "name": "value"
                    ,"password": data.password
                    ,"phone": data.phone
                    ,"id":data.id

                });
                layer.open({
                    title: "用户修改",
                    type: 1,
                    content: $("#updateUserForm"),
                    area: ['400px', '300px']
                });
                //动态加载所有的部门信息，填充
                //填充部门下拉框deptidSelect
                $.ajax({
                    url:"${pageContext.request.contextPath}/dept?method=findAllDept",
                    type:"POST",
                    dataType:"JSON",
                    success:function (ret) {
                        if(ret.code==0){
                            //<option value="0">写作</option>
                            var htmlStr="";
                            for (let i = 0; i < ret.data.length; i++) {
                                if(data.deptid==ret.data[i].deptid){
                                    //如果当前选中的行中的部门id和后台回传的集合中的部门id相等，选中这个部门
                                    htmlStr+="<option selected value="+ret.data[i].deptid+">"+ret.data[i].dname+"</option>"
                                }else
                                {
                                    htmlStr+="<option value="+ret.data[i].deptid+">"+ret.data[i].dname+"</option>"
                                }

                            }
                            console.log(htmlStr)
                            $("#deptidSelectUpdate").html(htmlStr);
                            //重新渲染表单
                            form.render('select');
                        }
                    }
                });
            }
        });

        form.verify({
            username: function(value, item){ //value：表单的值、item：表单的DOM对象
                if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                    return '用户名不能有特殊字符';
                }
                if(/(^\_)|(\__)|(\_+$)/.test(value)){
                    return '用户名首尾不能出现下划线\'_\'';
                }
                if(/^\d+\d+\d$/.test(value)){
                    return '用户名不能全为数字';
                }
            }

            //我们既支持上述函数式的方式，也支持下述数组的形式
            //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
            ,pass: [
                /^[\S]{6,12}$/
                ,'密码必须6到12位，且不能出现空格'
            ]
        });

    });

    //重置查询表单
    function resetSearch() {
        //清空查询表单输入框
        //刷新表格数据
        table.reload('demo', {
            where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
                search_username: "" //查询字段
                , search_phone: "" //排序方式

            },
            page: {
                curr: 1 //重新从第 1 页开始
            }

        });
    }

</script>
</html>
