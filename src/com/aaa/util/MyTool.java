package com.aaa.util;

import com.aaa.entity.Result;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/22 15:58
 * @description：我的工具类
 * @modified By：
 * @version: 1.0
 */
public class MyTool {

    /**
     * @create by: Teacher陈
     * @description: 操作成功时需要调用的方法
     * @create time: 2020/10/22 15:59
     * @param obj
     * @return Result
     */
    public static Result getSuccessReturn(Result obj){
        obj.setCode(ReturnStatus.SUCCESS.getCode());
        obj.setMsg(ReturnStatus.SUCCESS.getMessage());
        return obj;
    }
    /**
     * @create by: Teacher陈
     * @description: 操作失败时需要调用的方法
     * @create time: 2020/10/22 15:59
     * @param obj
     * @return Result
     */
    public static Result getFailedReturn(Result obj){
        obj.setCode(ReturnStatus.FAILED.getCode());
        obj.setMsg(ReturnStatus.FAILED.getMessage());
        return obj;
    }
}
