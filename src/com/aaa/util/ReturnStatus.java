package com.aaa.util;
/**
 * @create by: Teacher陈
 * @description: 返回状态
 * @create time: 2020/10/22 15:53
 */
public enum ReturnStatus {
    SUCCESS(0,"操作成功"),
    FAILED(1,"操作失败");
    private int code;
    private String message;

    ReturnStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
