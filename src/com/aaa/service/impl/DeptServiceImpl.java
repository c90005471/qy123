package com.aaa.service.impl;

import com.aaa.dao.DeptDao;
import com.aaa.dao.impl.DeptDaoImpl;
import com.aaa.service.DeptService;

import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/26 15:23
 * @description：部门业务的实现类
 * @modified By：
 * @version: 1.0
 */
public class DeptServiceImpl  implements DeptService {
    DeptDao deptDao= new DeptDaoImpl();
    @Override
    public List<Map<String, Object>> findAllDept() {
        return deptDao.findAllDept();
    }
}
