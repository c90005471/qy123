package com.aaa.service.impl;

import com.aaa.dao.UserInfoDao;
import com.aaa.dao.impl.UserInfoDaoImpl;
import com.aaa.service.UserInfoService;

import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/17 15:23
 * @description：用户业务接口实现类
 * @modified By：
 * @version: 1.0
 */
public class UserInfoSericeImpl  implements UserInfoService {
    UserInfoDao  userInfoDao= new UserInfoDaoImpl();
    @Override
    public List<Map<String, Object>> findAllUser(Map<String,String> map) {
        return userInfoDao.findAllUser( map);
    }

    @Override
    public int findAllUserCount(Map<String,String> map) {
        return userInfoDao.findAllUserCount(map);
    }

    @Override
    public boolean saveUser(Map<String, String> map) {
        return userInfoDao.saveUser(map);
    }

    @Override
    public boolean updateUser(Map<String, String> map) {
        return userInfoDao.updateUser(map);
    }

    @Override
    public boolean delUserById(Map<String, String> map) {
        return userInfoDao.delUserById(map);
    }
}
