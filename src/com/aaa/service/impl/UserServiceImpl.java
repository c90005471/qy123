package com.aaa.service.impl;

import com.aaa.dao.UserDao;
import com.aaa.dao.impl.UserDaoImpl;
import com.aaa.service.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/26 16:53
 * @description：用户业务实现类
 * @modified By：
 * @version: 1.0
 */
public class UserServiceImpl  implements UserService {
    UserDao userDao= new UserDaoImpl();
    @Override
    public List<Map<String, Object>> findAllUser(Map<String, String> map) {
        return userDao.findAllUser(map);
    }

    @Override
    public Map<String, Object> findAllMenuByLoginName(String loginName) {
        List<Map<String, Object>> menus = userDao.findAllMenuByLoginName(loginName);
        Map <String,Object>  menuMap=new HashMap<>();
        List<Map<String, Object>> firstMenus =new ArrayList<>();
        List<Map<String, Object>> secondMenus =new ArrayList<>();

        //获取一级菜单
        for (Map<String, Object> menu : menus) {
            if(menu.get("menu_type").equals("M")){
                firstMenus.add(menu);
            }
        }
        //获取二级菜单
        for (Map<String, Object> menu : menus) {
            if(menu.get("menu_type").equals("C")){
                secondMenus.add(menu);
            }
        }
        menuMap.put("firstMenus",firstMenus);
        menuMap.put("secondMenus",secondMenus);
        return menuMap;
    }
}
