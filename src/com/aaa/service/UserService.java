package com.aaa.service;

import java.util.List;
import java.util.Map;

/**
 * @create by: Teacher陈
 * @description: 用户dao接口
 * @create time: 2020/10/17 15:22
 */
public interface UserService {
    List<Map<String,Object>> findAllUser(Map<String, String> map);
    /**
     * @create by: Teacher陈
     * @description: 将dao层返回的所有权限菜单，重新封装，按照一级菜单二级菜单的分类返回给servlet
     * @create time: 2020/10/26 17:00
     * @param loginName
     * @return
     */
    Map<String,Object> findAllMenuByLoginName(String loginName);

}
