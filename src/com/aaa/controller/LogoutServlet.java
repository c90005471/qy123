package com.aaa.controller;

import cn.hutool.core.util.ObjectUtil;
import com.aaa.service.UserService;
import com.aaa.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/26 16:45
 * @description：注销servlet
 * @modified By：
 * @version: 1.0
 */
@WebServlet(name = "LogoutServlet" ,urlPatterns = "/userLogout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       //将session失效
        req.getSession().invalidate();
        //注销之后去登录页面
        req.getRequestDispatcher("login.jsp").forward(req,resp);

    }
}
