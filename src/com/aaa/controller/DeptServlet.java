package com.aaa.controller;

import cn.hutool.core.util.ObjectUtil;
import com.aaa.entity.Result;
import com.aaa.service.DeptService;
import com.aaa.service.UserInfoService;
import com.aaa.service.impl.DeptServiceImpl;
import com.aaa.service.impl.UserInfoSericeImpl;
import com.aaa.util.MyTool;
import com.aaa.util.ReturnStatus;
import com.alibaba.fastjson.JSON;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/17 15:19
 * @description：部门业务控制器111
 * @modified By：
 * @version: 1.0
 */
@WebServlet(name = "DeptServlet", urlPatterns = "/dept")
public class DeptServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //返回所有的请求参数
        Map<String, String[]> parameterMap = req.getParameterMap();
        //将 Map<String, String[]>转换成 Map<String, String>
        Map<String, String> myMap = new HashMap<>();
        //遍历原始map
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            if (ObjectUtil.isNotEmpty(entry.getValue())) {
                myMap.put(entry.getKey(), entry.getValue()[0]);
            }
        }
        String method = myMap.get("method") + "";
        DeptService deptService = new DeptServiceImpl();
        Result result = new Result();
        switch (method) {

            case "findAllDept":
                List<Map<String, Object>> allDept = deptService.findAllDept();
                result = MyTool.getSuccessReturn(result);
                result.setData(allDept);
                break;
        }
        String jsonString = JSON.toJSONStringWithDateFormat(result, "yyyy-MM-dd HH:mm:ss");
        resp.getWriter().print(jsonString);

    }


}
