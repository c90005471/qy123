package com.aaa.entity;

import java.util.List;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/17 15:29
 * @description：返回的结果实体类
 * @modified By：
 * @version: 1.0
 */
public class Result {
    private int code;
    private String msg;
    private int count;
    private List<?> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
