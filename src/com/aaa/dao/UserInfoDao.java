package com.aaa.dao;

import java.util.List;
import java.util.Map;

/**
 * @create by: Teacher陈
 * @description: 用户dao接口
 * @create time: 2020/10/17 15:22
 */
public interface UserInfoDao {
    List<Map<String,Object>> findAllUser(Map<String,String> map);
    int findAllUserCount(Map<String,String> map);
    boolean saveUser(Map<String,String> map);
    boolean updateUser(Map<String,String> map);
    boolean delUserById(Map<String,String> map);
}
