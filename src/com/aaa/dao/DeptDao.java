package com.aaa.dao;

import java.util.List;
import java.util.Map;

/**
 * @create by: Teacher陈
 * @description: 用户业务接口
 * @create time: 2020/10/17 15:22
 */
public interface DeptDao {
    List<Map<String,Object>> findAllDept();
}
