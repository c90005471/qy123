package com.aaa.dao.impl;

import com.aaa.dao.DeptDao;
import com.aaa.util.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/26 15:24
 * @description：部门dao实现类
 * @modified By：
 * @version:
 */
public class DeptDaoImpl  extends BaseDao implements DeptDao {
    @Override
    public List<Map<String, Object>> findAllDept() {
        String sql="select * from dept";
        return executeQuery(sql);
    }
}
