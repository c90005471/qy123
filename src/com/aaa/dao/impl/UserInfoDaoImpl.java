package com.aaa.dao.impl;

import cn.hutool.core.util.StrUtil;
import com.aaa.dao.UserInfoDao;
import com.aaa.util.BaseDao;
import com.aaa.util.MyConstants;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/17 15:24
 * @description：用户dao接口实现类
 * @modified By：
 * @version: 1.0
 */
public class UserInfoDaoImpl extends BaseDao implements UserInfoDao {
    @Override
    public List<Map<String, Object>> findAllUser(Map<String,String> map) {
        //不是所有的页面都需要条件查询 ，排序，分页

        //定义基础sql语句，没有（条件查询 ，排序，分页）
        StringBuffer sql=new StringBuffer("select userinfo.*,dept.dname from userinfo inner join dept on userinfo.deptid=dept.deptid ");
        //如果有条件，拼写条件，如何知道是否有条件
        return executeQueryCondition(map,sql.toString());
    }
    /**
     * @create by: Teacher陈
     * @description: 查询用户表一共有多少条数据
     * @create time: 2020/10/17 16:26
     * @return
     */
    @Override
    public int findAllUserCount(Map<String,String> map) {
        //定义基础sql语句，没有（条件查询 ，排序，分页）
        StringBuffer sql=new StringBuffer("select count(*) total from userinfo where 1=1 ");
        //如果有条件，拼写条件，如何知道是否有条件
        Map<String,String> whereCondtion= new HashMap<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if(entry.getKey().contains("search_")){
                whereCondtion.put(entry.getKey().replace("search_",""),entry.getValue());
            }
        }
        for (Map.Entry condition : whereCondtion.entrySet()) {
            sql.append( " and "+condition.getKey()+" like '%"+condition.getValue()+"%' ");
        }
        List<Map<String, Object>> mapList = executeQuery(sql.toString());
        if(mapList!=null&&mapList.size()>0){
            Map<String, Object> objectMap = mapList.get(0);
            Object total = objectMap.get("total");
            int count = Integer.parseInt(total + "");
            return count;
        }
        return 0;
    }

    @Override
    public boolean saveUser(Map<String, String> map) {
        String sql="insert into userinfo (username,password,createtime,phone,deptid) values (?,?,?,?,?)";

        return executeUpdate(sql,map.get("username"),map.get("password"),new Date(),map.get("phone"),map.get("deptid"));
    }

    @Override
    public boolean updateUser(Map<String, String> map) {
        String sql="update   userinfo  set username=?, password=?, createtime =? ,phone=? where id=?";

        return executeUpdate(sql,map.get("username"),map.get("password"),new Date(),map.get("phone"),map.get("id"));

    }

    /**
     * @create by: Teacher陈
     * @description: 支持单行和多行数据的删除
     * @create time: 2020/10/22 15:09
     * @return
     */
    @Override
    public boolean delUserById(Map<String, String> map) {
        String sql="delete from userinfo where id in ( "+ map.get("id")+" )";
        return executeUpdate(sql);
    }
}
