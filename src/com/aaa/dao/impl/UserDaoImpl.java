package com.aaa.dao.impl;

import com.aaa.dao.UserDao;
import com.aaa.util.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * @author ：Teacher陈
 * @date ：Created in 2020/10/26 16:50
 * @description：
 * @modified By：
 * @version:
 */
public class UserDaoImpl  extends BaseDao implements UserDao {
    @Override
    public List<Map<String, Object>> findAllUser(Map<String, String> map) {
        String sql="select * from tbl_user where login_name =? and password=?";
        return executeQuery(sql,map.get("username"),map.get("password"));
    }

    @Override
    public List<Map<String, Object>> findAllMenuByLoginName(String loginName) {
        String sql="SELECT DISTINCT\n" +
                "\tm.* \n" +
                "FROM\n" +
                "\t(\n" +
                "\tSELECT\n" +
                "\t\trm.menu_id \n" +
                "\tFROM\n" +
                "\t\ttbl_role_menu rm \n" +
                "\tWHERE\n" +
                "\t\trm.role_id IN (\n" +
                "\t\tSELECT\n" +
                "\t\t\tur.role_id \n" +
                "\t\tFROM\n" +
                "\t\t\ttbl_user_role ur \n" +
                "\t\tWHERE\n" +
                "\t\tur.user_id = ( SELECT user_id FROM tbl_user WHERE login_name = ? ))) mm\n" +
                "\tLEFT JOIN tbl_menu m ON mm.menu_id = m.menu_id";
        return executeQuery(sql,loginName);
    }
}
